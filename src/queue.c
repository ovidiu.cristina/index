#include "queue.h"

queue *initQueue(){
    queue *ans = malloc(sizeof(queue));
    ans -> head = ans -> tail = NULL;
    return ans;
}

point *initPoint(pair d){
    point *ans = malloc(sizeof(point));
    ans -> data = d;
    ans -> next = NULL;

    return ans;
}

void push(queue *q,pair d){
    if(q->head == NULL){
        q->head = q->tail = initPoint(d);
    }
    else{
        point *p = initPoint(d);
        q -> tail -> next = p;
        q -> tail = p;
    }
}

pair pop(queue *q){
    assert(q->head != NULL);
    
    point *p = q -> head;
    pair ans = p -> data;

    q->head = q->head->next;
    free(p);

    return ans;
}

bool isEmpty(queue *q){
    return q->head == NULL;
}

pair make_pair(long n,unsigned level){
    return (pair){.n = n,.level = level};
}