#include "btree.h"
#include <stdlib.h>
#include <assert.h>

static node* initNode(tree *bt){
    unsigned t = bt -> t;

    node* ans = malloc(sizeof(node));
    assert(ans != NULL);

    ans -> keys = initKey((2 * t - 1),bt->keysize);
    ans -> vals = malloc((2 * t - 1) * sizeof(long));
    ans -> child = malloc(2 * t * sizeof(long));

    assert(ans -> vals != NULL);
    assert(ans -> child != NULL);

    ans -> n = 0;
    ans -> leaf = true;

    ans->offset = -1;

    return ans;
}

static void delNode(node *x){
    if(x == NULL) return;
    free(x -> keys.k);
    free(x -> vals);
    free(x -> child);
    free(x);
}

static void serializeNode(tree *bt,node *x){
    memcpy(bt->dbbuf,&(x->n),sizeof(x->n));
    int i = sizeof(x->n);

    memcpy(bt->dbbuf + i,&(x->leaf),sizeof(x->leaf));
    i += sizeof(x->leaf);
    
    //keys
    memcpy(bt->dbbuf + i,x->keys.k,x->n * bt->keysize);
    i += (bt->t * 2 - 1) * bt->keysize;
    memcpy(bt->dbbuf + i,&(x->keys.n),ssizeof(key,n));
    i += ssizeof(key,n);
    memcpy(bt->dbbuf + i,&(x->keys.keysize),ssizeof(key,keysize));
    i += ssizeof(key,keysize);

    memcpy(bt->dbbuf + i,x->vals,x->n * sizeof(long));
    i += (bt->t * 2 - 1) * sizeof(long);

    memcpy(bt->dbbuf + i,x->child,(x->n + 1) * sizeof(long));
    i += (bt->t * 2) * sizeof(long);

    memcpy(bt->dbbuf + i,&(x->offset),sizeof(long));
}

static void deserializeNode(tree *bt,node *x){
    memcpy(&(x->n),bt->dbbuf,sizeof(x->n));
    int i = sizeof(x->n);

    memcpy(&(x->leaf),bt->dbbuf + i,sizeof(x->leaf));
    i += sizeof(x->leaf);

    //keys
    memcpy(x->keys.k,bt->dbbuf + i,x->n * bt->keysize);
    i += (bt->t * 2 - 1) * bt->keysize;
    memcpy(&(x->keys.n),bt->dbbuf + i,ssizeof(key,n));
    i += ssizeof(key,n);
    memcpy(&(x->keys.keysize),bt->dbbuf + i,ssizeof(key,keysize));
    i += ssizeof(key,keysize);

    memcpy(x->vals,bt->dbbuf + i,(x->n * sizeof(long)));
    i += (bt->t * 2 - 1) * sizeof(long);

    memcpy(x->child,bt->dbbuf + i,(x->n + 1) * sizeof(long));
    i += (bt->t * 2) * sizeof(long);

    memcpy(&(x->offset),bt->dbbuf + i,sizeof(long));

}

static long saveNode(tree *bt,node *x){
    long ans;

    if(x->offset < 0){
        fseek(bt->db->index,0,SEEK_END);
        x->offset = ftell(bt->db->index);
    }
    else fseek(bt->db->index,x->offset,SEEK_SET);

    serializeNode(bt,x);
    
    ans = x->offset;

    if(!fwrite(bt->dbbuf,bt->totalNodeSize,1,bt->db->index)){
        perror("fwrite(): error writing to disk\n");
        exit(1);
    }

    delNode(x);

    return ans;
}

static node *loadNode(tree *bt,long offset){
    node *ans = initNode(bt);

    fseek(bt->db->index,offset,SEEK_SET);

    if(!fread(bt->dbbuf,bt->totalNodeSize,1,bt->db->index)){
        perror("fread(): error reading from disk\n");
        exit(1);
    }

    deserializeNode(bt,ans);

    return ans;
}

static tree* allocTree(unsigned t,unsigned keysize,unsigned recordsize,db *data){
    tree *bt = malloc(sizeof(tree));
    bt->t = t;
    bt->keysize = keysize;
    bt->recordsize = recordsize;
    bt->db = data;

    bt->totalNodeSize = ssizeof(node,n) + ssizeof(node,leaf) +
                        bt->keysize * (bt->t * 2 - 1) +
                        ssizeof(key,n) +
                        ssizeof(key,keysize) +
                        sizeof(long) * (bt->t * 2 - 1) +
                        sizeof(long) * (bt->t * 2) +
                        ssizeof(node,offset);

    bt->dbbuf = malloc(bt->totalNodeSize);
    bt->databuf = malloc(recordsize - keysize);

    assert(bt->dbbuf != NULL);
    assert(bt->databuf != NULL);

    return bt;
}

tree* initTree(int t,int keysize,int recordsize,db *data){
    tree *bt = allocTree(t,keysize,recordsize,data);

    node *root = initNode(bt);

    bt->root = saveNode(bt,root);

    return bt;
}

void saveTree(tree *bt){
    __ssize_t size = ssizeof(tree,t) + 
                    ssizeof(tree,keysize) + ssizeof(tree,recordsize) +
                    ssizeof(tree,root);
    unsigned char buf[size];

    memcpy(buf,&(bt->t),ssizeof(tree,t));
    int i = ssizeof(tree,t);

    memcpy(buf + i,&(bt->keysize),ssizeof(tree,keysize));
    i += ssizeof(tree,keysize);

    memcpy(buf + i,&(bt->recordsize),ssizeof(tree,recordsize));
    i += ssizeof(tree,recordsize);

    memcpy(buf + i,&(bt->root),ssizeof(tree,root));

    fseek(bt->db->index,2,SEEK_SET);
    if(!fwrite(buf,size,1,bt->db->index)){
        perror("fwrite(): error writing to disk\n");
        exit(1);
    }

    delTree(bt);
}

tree* loadTree(db *data){
    __ssize_t size = ssizeof(tree,t) + 
                    ssizeof(tree,keysize) + ssizeof(tree,recordsize) +
                    ssizeof(tree,root);
    unsigned char buf[size];

    fseek(data->index,2,SEEK_SET);
    if(!fread(buf,size,1,data->index)){
        perror("fread(): error reading from disk\n");
        exit(1);
    }

    unsigned t,keysize,recordsize;
    long root;

    memcpy(&t,buf,ssizeof(tree,t));
    int i = ssizeof(tree,t);

    memcpy(&keysize,buf + i,ssizeof(tree,keysize));
    i += ssizeof(tree,keysize);

    memcpy(&recordsize,buf + i,ssizeof(tree,recordsize));
    i += ssizeof(tree,recordsize);

    memcpy(&root,buf + i,ssizeof(tree,root));

    tree *bt = allocTree(t,keysize,recordsize,data);
    
    bt->root = root;

    return bt;
}

void delTree(tree *bt){
    free(bt->dbbuf);
    free(bt->databuf);
    free(bt);
}

static void split(tree *bt,node *x,int pos){
    node *y = loadNode(bt,x->child[pos]);
    node *z = initNode(bt);

    z -> leaf = y -> leaf;

    unsigned t = bt -> t;
    unsigned i;

    for(i = 0;i < t - 1; ++i){
        cpyKey(&z->keys,i,&y->keys,t + i);
        z->vals[i] = y->vals[t + i];
    }

    for(i = 0;i < t; ++i){
        z->child[i] = y->child[t + i];
    }

    z->n = y->n = t - 1;

    for(int i = x->n;i > pos;--i){
        x->child[i + 1] = x->child[i];
    }
    x->child[pos + 1] = saveNode(bt,z);

    for(int i = x->n - 1;i >= pos;--i){
        cpyKey(&x->keys,i + 1,&x->keys,i);
        x->vals[i + 1] = x->vals[i];
    }

    cpyKey(&x->keys,pos,&y->keys,t - 1);
    x->vals[pos] = y->vals[t - 1];
    ++x->n;

    saveNode(bt,y);
}

static void insertNonFull(tree *bt,long ax,unsigned char *key,long val){
    node *x = loadNode(bt,ax);
    int i;

    if(x -> leaf){
        for(i = x->n - 1;i >= 0 && cmpKey(&x->keys,i,key) > 0; --i){
            cpyKey(&x->keys,i + 1,&x->keys,i);
            x->vals[i + 1] = x->vals[i];
        }
        ++i;
        cpykey(&x->keys,i,key);
        x->vals[i] = val;
        ++x->n;
    }
    else{
        for(i = x->n - 1;i >= 0 && cmpKey(&x->keys,i,key) > 0; --i);
        ++i;

        node *y = loadNode(bt,x->child[i]);
        if(y->n == 2 * bt->t - 1){
            split(bt,x,i);
            if(cmpKey(&x->keys,i,key) < 0){
                ++i;
            }
        }
        delNode(y);
        insertNonFull(bt,x->child[i],key,val);
    }

    saveNode(bt,x);
}

void put(tree *bt,unsigned char *key,long val){
    node *r = loadNode(bt,bt->root);
    if(r -> n == 2 * bt -> t - 1){
        node *s = initNode(bt);
        s->leaf = false;
        s->n = 0;
        s->child[0] = bt->root;

        split(bt,s,0);
        bt->root = saveNode(bt,s);
        insertNonFull(bt,bt->root,key,val);
    }
    else{
        insertNonFull(bt,bt->root,key,val);
    }
    delNode(r);
}

unsigned char *cloneKey(const unsigned char *key,int keysize){
    unsigned char *ans = malloc(keysize);
    memcpy(ans,key,1);
    return ans;
}

static long _get(tree *bt,long ax,unsigned char *key){
    node *x = loadNode(bt,ax);
    int i;
    for(i = 0;i < x->n && cmpKey(&x->keys,i,key) < 0;++i);

    int j = i;
    if(j == x->n) --j;


    if(x->leaf){
        if(cmpKey(&x->keys,j,key) == 0){
            return x->vals[j];
        }
        return -1;
    }
    else{
        if(cmpKey(&x->keys,j,key) == 0){
            return x->vals[j];
        }
        return _get(bt,x->child[i],key);
    }
    delNode(x);
}

long get(tree *bt,unsigned char *key){
    return _get(bt,bt->root,key);
}

bool contains(tree *bt,unsigned char *key){
    return get(bt,key) > -1;
}

static long putData(tree *bt,unsigned char *key,unsigned char *val){
    fseek(bt->db->data,0,SEEK_END);
    long off = ftell(bt->db->data);

    if(!fwrite(key,bt->keysize,1,bt->db->data)){
        perror("fwrite(): error writing to disk\n");
        exit(1);
    }

    if(!fwrite(val,bt->recordsize - bt->keysize,1,bt->db->data)){
        perror("fwrite(): error writing to disk\n");
        exit(1);
    }

    return off;
}

static unsigned char *getData(tree *bt,long offset){
    fseek(bt->db->data,offset + (long)bt->keysize,SEEK_SET);

    if(!fread(bt->databuf,bt->recordsize - bt->keysize,1,bt->db->data)){
        perror("fread(): error reading from disk\n");
        exit(1);
    }

    return bt->databuf;
}

unsigned char *getRecord(tree *bt,unsigned char *key){
    long off = get(bt,key);
    
    if(off == -1) return NULL;
    
    return getData(bt,off);
}

void putRecord(tree *bt,unsigned char *key,unsigned char *val){
    if(contains(bt,key)){
        printf("Record already stored!\nUse updaterecord command instead.\n");
        return;
    }
    
    long off = putData(bt,key,val);

    put(bt,key,off);
}

void updateRecord(tree *bt,unsigned char *key,unsigned char *val){
    long off = _get(bt,bt->root,key);

    fseek(bt->db->data,off,SEEK_SET);

    if(!fwrite(key,bt->keysize,1,bt->db->data)){
        perror("fwrite(): error writing to disk\n");
        exit(1);
    }

    if(!fwrite(val,bt->recordsize - bt->keysize,1,bt->db->data)){
        perror("fwrite(): error writing to disk\n");
        exit(1);
    }
}

static void printNode(node *x,FILE *f){
    if(x == NULL){
        return;
    }

    fprintf(f,"||");
    for(int i = 0;i < x->n;++i){
        fprintf(f,"%c->%ld|",getKey(&x->keys,i)[0],x->vals[i]);
    }
    fprintf(f,"|  ");
}

void levels(tree *bt,FILE *f){
    queue *q = initQueue();

    push(q,make_pair(bt->root,0));

    node *x;
    pair p;
    unsigned level,lastLevel = 0;

    while(!isEmpty(q)){
        p = pop(q);
        x = loadNode(bt,p.n);
        level = p.level;

        if(level != lastLevel){
            fprintf(f,"\n");
            lastLevel = level;
        }

        printNode(x,f);
        
        if(!x->leaf){
            for(int i = 0;i <= x->n;++i){
                push(q,make_pair(x->child[i],level + 1));
            }
        }
    }
    fprintf(f,"\n");

    free(q);
}