#include "utils.h"

void clearBuf(FILE *f){
    char c = getc(f);
    while(isspace(c)){
        c = getc(f);
    }
    ungetc(c,f);
}

arg extractArgs(char *command){
    int s = BUF_SIZE;
    arg ans;
    ans.argc = 0;
    ans.argv = malloc(s * sizeof(char*));

    char *aux = strtok(command," ");
    while(aux != NULL){
        ans.argv[ans.argc++] = strdup(aux);
        aux = strtok(NULL," ");

        if(ans.argc == s){
            s += BUF_SIZE;
            ans.argv = realloc(ans.argv,s);
        }
    }

    return ans;
}

void delArg(arg *x){
    for(int i = 0;i < x->argc;++i){
        free(x->argv[i]);
    }
    free(x->argv);
}

command_t convert(const char *c){
    if(!strcmp(c,"createindex"))    return CREATEINDEX;
    if(!strcmp(c,"showindex"))      return SHOWINDEX;
    if(!strcmp(c,"addrecord"))      return ADDRECORD;
    if(!strcmp(c,"searchrecord"))   return SEARCHRECORD;
    if(!strcmp(c,"updaterecord"))   return UPDATERECORD;
    return INVALIDCOMMAND;
}