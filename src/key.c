#include "key.h"

key initKey(unsigned n,unsigned keysize){
    key ans;
    ans.n = n;
    ans.keysize = keysize;
    ans.k = malloc(n * keysize);

    assert(ans.k != NULL);

    return ans;
}

unsigned char *getKey(const key *k,unsigned pos){
    assert(pos < k->n);

    pos *= k->keysize;
    return k->k + pos;
}

void cpykey(key *dst,unsigned pd,const unsigned char *k2){
    assert(pd < dst->n);

    memcpy(getKey(dst,pd),k2,dst->keysize);
}

void cpyKey(key *dst,unsigned pd,const key *src,unsigned ps){
    assert(dst->keysize == src->keysize);
    assert(pd < dst->n);
    assert(ps < src->n);

    memcpy(getKey(dst,pd),getKey(src,ps),dst->keysize);
}

int cmpkey(const unsigned char *k1,const unsigned char *k2,unsigned keysize){
    return memcmp(k1,k2,keysize);
}

int cmpKey(const key *k1,unsigned pos,const unsigned char *k2){
    assert(pos < k1->n);

    return cmpkey(getKey(k1,pos),k2,k1->keysize);
}