#include "db.h"

static char *getIndexPath(const char *index){
    int len = strlen(index);
    char *ans = malloc(len + 5);
    strcpy(ans,index);
    strcpy(ans + len,".ndx");

    return ans;
}

db initDB(const char *path,const char *modeData,const char *modeIndex){
    db ans;

    ans.path = strdup(path);
    ans.indexPath = getIndexPath(ans.path);

    ans.data = fopen(ans.path,modeData);
    if(ans.data == NULL){
        perror("fopen(): ");
        exit(1);
    }

    ans.index = fopen(ans.indexPath,modeIndex);
    if(ans.index == NULL){
        perror("fopen(): ");
        exit(1);
    }

    return ans;
}

void clearDB(db *d){
    d->index = fopen(d->indexPath,"w");
    fclose(d->index);
    d->index = fopen(d->indexPath,"r+");
}

void closeDB(db *d){
    free(d->path);
    free(d->indexPath);
    fclose(d->data);
    fclose(d->index);
}