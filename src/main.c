#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "commands.h"

int main(int argc,char**argv){
    if(argc < 2){
        printf("Usage:\n%s <command> <options>\tOR\n%s <file>\n",argv[0],argv[0]);
        exit(1);
    }

    // if(argc == 2){
    //     char *path = argv[1];

    //     //check if command was used in real-time mode (the second argument was a file path)
    //     if(!access(path,F_OK)){
    //         realtime(path);
    //         return 0;
    //     }
    // }

    switch(convert(argv[1])){
        case CREATEINDEX:
            if(argc < 5){
                printf("Usage: %s %s <datafile> <recordsize> <keysize>\n",argv[0],argv[1]);
                exit(1);
            }
            
            unsigned keysize = atoi(argv[4]);
            unsigned recordsize = atoi(argv[3]);

            createindex(argv[2],recordsize,keysize,4096);
            break;

        case SHOWINDEX:
            if(argc < 3){
                printf("Usage: %s %s <datafile>\n",argv[0],argv[1]);
                exit(1);
            }

            showindex(argv[2]);
            break;
        
        case ADDRECORD:
            if(argc < 5){
                printf("Usage: %s %s <datafile> <key> <value>\n",argv[0],argv[1]);
                exit(1);
            }
            
            addrecord(argv[2],(unsigned char*)argv[3],(unsigned char*)argv[4]);
            break;

        case SEARCHRECORD:
            if(argc < 4){
                printf("Usage: %s %s <datafile> <key>\n",argv[0],argv[1]);
                exit(1);
            }

            searchrecord(argv[2],(unsigned char*)argv[3]);
            break;
        
        case UPDATERECORD:
            if(argc < 5){
                printf("Usage: %s %s <datafile> <key> <value>\n",argv[0],argv[1]);
                exit(1);
            }
            
            updaterecord(argv[2],(unsigned char*)argv[3],(unsigned char*)argv[4]);
            break;
            
        default:
            printf("Invalid command!\n");
            exit(1);
    }

    return 0;
}