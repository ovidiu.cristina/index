#include "commands.h"

void createindex(const char *path,unsigned recordsize,unsigned keysize,int t){
    printf("createindex: %s %d %d\n",path,recordsize,keysize);

    db data = initDB(path,"r","w");
    clearDB(&data);

    unsigned char *buff = malloc(recordsize);
    unsigned char *key = malloc(keysize);
    long offset = 0;

    /* FIRST RECORD IN INDEX FILE
        - 2 bytes for magic number (used to check if file is index)
        - space to store the tree structure (empty for now, will be completed by saveTree())
    */
    __ssize_t size = 2 +
                    ssizeof(tree,t) + 
                    ssizeof(tree,keysize) + ssizeof(tree,recordsize) +
                    ssizeof(tree,root);
    unsigned char buf[size];
    *(__uint16_t*)buf = NDX_MAGIC_NUMBER; //MAGIC NUMBER FOR INDEX FILE
    fwrite(buf,1,size,data.index);

    tree *bt = initTree(t,keysize,recordsize,&data);
    bt->db = &data;

    while (fread(buff,recordsize,1,data.data)){
        memcpy(key,buff,keysize);
        put(bt,key,offset);

        offset += recordsize;
    }

    saveTree(bt);

    closeDB(&data);

    free(buff);
    free(key);

    puts("Done!");
}

void showindex(const char *path){
    printf("showindex: %s\n",path);

    db data = initDB(path,"r","r");
    tree *bt = loadTree(&data);

    levels(bt,stdout);

    closeDB(&data);
    delTree(bt);
}

void addrecord(const char *path,unsigned char *key,unsigned char *val){
    printf("addrecord: %s %c %c\n",path,key[0],val[0]);

    db data = initDB(path,"r+","r+");
    tree *bt = loadTree(&data);

    putRecord(bt,key,val);

    saveTree(bt);

    closeDB(&data);
}

void searchrecord(const char *path,unsigned char *key){
    printf("searchrecord: %s %c\n",path,key[0]);

    db data = initDB(path,"r","r");
    tree *bt = loadTree(&data);

    printf("%31s\n",getRecord(bt,key));

    closeDB(&data);
    delTree(bt);
}

void updaterecord(const char *path,unsigned char *key,unsigned char *val){
    printf("updaterecord: %s %c %c\n",path,key[0],val[0]);

    db data = initDB(path,"r+","r+");
    tree *bt = loadTree(&data);

    updateRecord(bt,key,val);

    closeDB(&data);
    delTree(bt);
}

void realtime(const char *path){
    printf("realtime: %s\n",path);

    db data = initDB(path,"r+","r+");
    tree *bt = loadTree(&data);

    __ssize_t maxs = bt->keysize + bt->recordsize + 100;
    unsigned len;
    char *line = malloc(maxs);
    arg args;

    while(1){
        printf("> ");

        fgets(line,maxs,stdin);
        len = strlen(line);

        if(line[len - 1] != '\n'){
            printf("Command is too long!\n");
            continue;
        }

        args = extractArgs(line);

        if(args.argc == 0) continue;

        printf("%d\n%s\n%s\n%s",args.argc,args.argv[0],args.argv[1],args.argv[2]);
    }

    closeDB(&data);
    delTree(bt);
    free(line);
    delArg(&args);
}