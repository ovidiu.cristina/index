#ifndef __UTILS_H
#define __UTILS_H

#include <ctype.h>

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <assert.h>

#define BUF_SIZE    8

typedef struct{
    int argc;
    char **argv;
}arg;

typedef enum{
    CREATEINDEX,
    SHOWINDEX,
    ADDRECORD,
    SEARCHRECORD,
    UPDATERECORD,
    INVALIDCOMMAND,
}command_t;

void clearBuf(FILE *f);

arg extractArgs(char *command);

void delArg(arg *x);

command_t convert(const char *c);

#endif