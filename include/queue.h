#ifndef __QUEUE_H
#define __QUEUE_H

#include "utils.h"

typedef struct{
    long n;
    unsigned level;
}pair;

typedef struct _point{
    pair data;
    struct _point *next;
}point;

typedef struct _queue{
    point *head,*tail;
}queue;

queue *initQueue();

void push(queue *q,pair p);

pair pop(queue *q);

bool isEmpty(queue *q);

pair make_pair(long n,unsigned level);

#endif