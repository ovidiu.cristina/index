#ifndef __KEY_H
#define __KEY_H

#include <stdlib.h>
#include <assert.h>
#include <string.h>

typedef struct{
    unsigned n,keysize;
    unsigned char *k;
}key;

key initKey(unsigned n,unsigned keysize);

void cpyKey(key *dst,unsigned pd,const key *src,unsigned ps);


unsigned char *getKey(const key *k,unsigned pos);

void cpykey(key *dst,unsigned pd,const unsigned char *k2);

void cpyKey(key *dst,unsigned pd,const key *src,unsigned ps);

int cmpkey(const unsigned char *k1,const unsigned char *k2,unsigned keysize);

int cmpKey(const key *k1,unsigned pos,const unsigned char *k2);

#endif