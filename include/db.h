#ifndef __DB_H
#define __DB_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NDX_MAGIC_NUMBER 0xDBDB

typedef struct{
    char *path,*indexPath;
    FILE *data,*index;
}db;

db initDB(const char *path,const char *modeData,const char *modeIndex);

void closeDB(db *d);

void clearDB(db *d);

#endif