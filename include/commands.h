#ifndef __COMMANDS_H
#define __COMMANDS_H

#include "btree.h"
#include "db.h"
#include <unistd.h>

void createindex(const char *path,unsigned recordsize,unsigned keysize,int t);

void showindex(const char *path);

void addrecord(const char *path,unsigned char *key,unsigned char *val);

void searchrecord(const char *path,unsigned char *key);

void updaterecord(const char *path,unsigned char *key,unsigned char *val);

void realtime(const char *path);

#endif