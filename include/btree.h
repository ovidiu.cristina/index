#ifndef __BTREE_H
#define __BTREE_H

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "utils.h"
#include "key.h"
#include "db.h"
#include "queue.h"

#define ssizeof(type,member) (sizeof(((type*)0)->member))

typedef struct{
    unsigned n;
    bool leaf;
    key keys;
    long *vals;
    long *child;

    long offset;
}node;

typedef struct{
    unsigned t;
    unsigned keysize,recordsize;
    unsigned totalNodeSize;
    long root;

    unsigned char *dbbuf;
    unsigned char *databuf;

    db *db;
}tree;

tree* initTree(int t,int keysize,int recordsize,db *data);

void saveTree(tree *bt);

tree* loadTree(db *data);

void delTree(tree *bt);

void put(tree *bt,unsigned char *key,long val);
long get(tree *bt,unsigned char *key);
bool contains(tree *bt,unsigned char *key);

unsigned char *getRecord(tree *bt,unsigned char *key);
void putRecord(tree *bt,unsigned char *key,unsigned char *val);
void updateRecord(tree *bt,unsigned char *key,unsigned char *val);

void levels(tree *bt,FILE *f);

#endif