# index



## Project's repo

https://gitlab.upt.ro/ovidiu.cristina/index

## Setup and usage

```
make
./index.x <command> <args>
           createindex datafile recordsize keysize
           searchrecord datafile key
           addrecord datafile key val
           updaterecord datafile key val
```